# MMI v2.0
# Codename: Fir
# Copyright 2021 Fe-Ti
"""!
Nessesary text structure classes for parser module
"""

import sys
import hashlib
import mimetypes as mts
import zipfile as z
from . import mmi_auto_styles as ast
from .mmi_config import MMIC
from .mmi_path_handler import PathHandler
from . import mmi_odtgenlib as ogl

def log(self, string):
    if self.debug:
        print(string)

def log_fname(self):
    if self.debug:
        print("Entered:", sys._getframe(1).f_code.co_name)


OOBJ_TYPE = "OObject"
HEAD_TYPE = "Heading"
TEXT_TYPE = "TextBody"
CODE_TYPE = "Code"
LIST_TYPE = "List"
BUL_LIST_TYPE = "ListBulleted"
NUM_LIST_TYPE = "ListNumbered"
PIC_TYPE = "Picture"
PAGE_BREAK_TYPE = "PageBreak"
ARBITRARY_TEXT_TYPE = "ArbitraryText"
FRAME_TYPE = "Frame"
LIST_ITEM_TYPE = "ListItem"
SPACE = ' '
EMPTY_STR = ''

TROW_TYPE = "TableRow"
TABLE_TYPE = "Table"

def is_heading(mmic, line):
    """!
    Check if LINE is marked as heading (by default a streak of '#').
    """
    if mmic.heading_trig in line:
        trig_pos = line.find(mmic.heading_trig)
        trig_end = mmic.heading_trig + SPACE
        char_after_trig = line[line.find(trig_end , trig_pos) + len(mmic.heading_trig)]
        if (line[0:trig_pos].isspace() or trig_pos == 0) and char_after_trig.isspace():
            return True
    return False


def is_picture(mmic, line): # if some trigger isn't found then we're searching from -1
    """!
    Check if LINE is marked as picture (by default a ![name](path)).
    """
    b_trig_pos = line.find(mmic.pic_b_trig)
    sl = line.find(mmic.pic_e_trig, line.find(mmic.pic_m_trig,
                                            b_trig_pos)) + len(mmic.pic_e_trig)
    # finally adding pic_e_trig length
    prefix = line[:b_trig_pos]
    if sl > mmic.pic_trig_len and prefix.isspace() or prefix == '':
        return True         # Even if len() of m_trig and b_trig is equal to 0
    return False            #   pic_trig_len is greater than sl.
                            # So we consider line to be a picture
                            #   only if we've found picture trigger, i.e. ![]( )


def is_page_break(mmic, line):
    """!
    Check if LINE is marked as a page break.
    """
    counter = 0
    for ch in line.replace(' ',''):
        if ch != mmic.page_break_trig[0]:   #As we're immitating Md we just need
            return False                    # to check only one character and if
                                            #  it's unexpected there is no PB
        else:
            counter += 1
            if counter >= len(mmic.page_break_trig):
                # If there are enough symbols say OK. That leads to smth like:
                return True # -- -- -lol i'm page break
                            # But who cares
    return False

def is_table(mmic, line):
    line = line.strip()
    if line.startswith(mmic.table_trig):
        return True
    return False

def is_bulleted_list_item(mmic, line):
    """!
    Check if LINE is marked as a bulleted list item.
    """
    trig_pos = line.find(mmic.bul_list_trig)
    return (line[0:trig_pos].isspace() or trig_pos == 0)


def is_numbered_list_item(mmic, line):
    """!
    Check if LINE is marked as a numbered list item.
    """
    return (line[0:line.find(mmic.num_list_trig)].lstrip().isdigit())


def is_indented(mmic, line):
    """!
    Check if LINE is marked as a page break.
    """
    return (line[:mmic.tab_size].isspace())


def get_indent_lvl(mmic, line): # slow but steady
    counter = 0
    while line[:mmic.tab_size].isspace():
        line = line[mmic.tab_size:]
        counter += 1
    return counter


def get_heading_lvl(mmic, trig_streak):
    return 1 + mmic.heading_lvl.index(trig_streak)


def rm_trig(line, trig, count=1):
    return line.replace(trig, '', count)

def save_spacing(line):
    counter = 0
    i = 0
    b_pos = i
    line = line.rstrip()
    while i < len(line):
        if line[i] == ' ':
            if counter == 0:
                b_pos = i
            counter += 1
        else:
            if counter > 1:
                line = line[:b_pos] + f'<text:s text:c="{counter}"/>' + line[b_pos + counter:]
                i = 0
            elif counter > 0 and b_pos == 0:
                line = line[:b_pos] + '<text:s/>' + line[b_pos + counter:]
                i = 0
            counter = 0
        i += 1
    return line

def replace_special_characters(mmic, line, escape_char = '\\'):
    line = line.replace(escape_char+'<','&lt;')
    line = line.replace(escape_char+'>','&gt;')
    return line

def process_pic_links(mmic, line):
    pic_link_trig = mmic.pic_link_trig
    while pic_link_trig in line:
        plt_pos = line.find(pic_link_trig)
        number_begin = plt_pos + len(pic_link_trig)
        number_end = number_begin
        while line[number_end].isdigit():
            number_end += 1
        number = int(line[number_begin:number_end])
        replment = ogl.mkpicture_link(number)
        line = line[:plt_pos] + replment + line[number_end:]
    return line

def prepare_text(mmic, line, is_code = False):
    """!
    Here the string is prepared, i.e. double quotes and special chars are
    replaced (if the line is not a code block, the special characters
    need to be escaped).
    """
    line = line.replace('&','&amp;') # we'll not support any html codes
    if not is_code:
        line = replace_special_characters(mmic, line)
        line = line.strip()
        if mmic.replace_hyphens:
            line = line.replace(' - ', mmic.hyphenrpl)
        if mmic.replace_dquotes:    # Replacing double quotes
            d = 1
            while mmic.dquote in line:
                if d > 0:
                    line = line.replace(mmic.dquote, mmic.ldquoterpl, 1)
                else:
                    line = line.replace(mmic.dquote, mmic.rdquoterpl, 1)
                d *= -1
        line = process_pic_links(mmic, line)
    else:
        line = replace_special_characters(mmic, line, escape_char='')
        if mmic.save_code_spaces:
            line = save_spacing(line)
    return line


class OObject: # abstract class
    """!
    An abstract class which implements all of the common functions.
    """
    lvl = 0
    obj_type = OOBJ_TYPE
    debug = MMIC.debug

    def __init__(self, mmic, nested_items = list()):
        log(self, f"Entered the __init__ of {self.obj_type}")
        self.nested_items = list()
        self.mmic = mmic # MMI config reference
        #print(nested_items)
        if type(nested_items) == type(list()):
            self.nested_items = nested_items[:]
        elif type(nested_items) == type(str()) and nested_items != "":
            self.nested_items.append(ArbitraryText(self.mmic, nested_items))
        else:
            self.nested_items.append(nested_items)
        log(self, f"Left the __init__ of {self.obj_type}")

    def __str__(self):
        """!
        Return a string representation of an object.
        """
        string = ""
        #print(self.obj_type, self.nested_items)
        for i in self.nested_items:
            string += str(i)
        return string

    # ~ def obj_type(self):
        # ~ """!
        # ~ Return an object type (used in parcing lists).
        # ~ """
        # ~ return self.obj_type

    def add_item(self, line, type_of_list, lvl):
        ## this function is only defined for lists
        pass

    def append_paragraph(self, line):
        ## this function is only defined for lists
        pass

    def append_line(self, line):
        """!
        Append either an ArbitraryText or delegate the line to the inner object.
        """
        if not self.nested_items:
            self.nested_items.append(ArbitraryText(self.mmic, line))
        else:
            self.nested_items[-1].append_line(line)
    def __repr__(self):
        return f"{self.obj_type}"

class ArbitraryText(OObject):
    """!
    There is only text with no nested items.
    """
    nested_items = list()
    obj_type = ARBITRARY_TEXT_TYPE

    def __init__(self, mmic, text = "", is_code = False):
        self.mmic = mmic
        self.text = prepare_text(self.mmic, text, is_code)

    def __str__(self):
        return self.text

    def append_line(self, line):
        """!
        Simply add a line to the self.text.
        There is no is_code parameter because the CodeBlock instances are single
        line paragraphs so this function is never used in them.
        """
        self.text = self.text + ' ' + prepare_text(self.mmic, line)
    def __repr__(self):
        return f"{self.obj_type}:{self.text}"

class TextParagraph(OObject):
    """!
    A TextParagraph can contain ArbitraryText and (to be implemented) SpanText.
    """
    obj_type = TEXT_TYPE
    def __init__(self, mmic, nested_items = list(), stylename = ''):
        # TODO: add parsing of embedded items
        if not stylename:
            self.stylename = mmic.text_body_stylename
        else:
            self.stylename = stylename
        super().__init__(mmic, nested_items)

    def __str__(self):
        """
        Return either a string representation of nested_items or empty <text:p>.
        """
        string = super().__str__()
        if not string:
            return ogl.mkemptytextp(self.stylename)
        else:
            return ogl.mktextp(self.stylename, string)


class CodeBlock(TextParagraph):
    """!
    A special text paragraph which contains preformatted text (i.e. it's left
    as it is in the editor).
    """
    obj_type = CODE_TYPE


    def __init__(self, mmic, nested_items):
        self.nested_items = list()
        self.stylename = mmic.code_stylename
        self.mmic = mmic
        self.nested_items.append(ArbitraryText(self.mmic, nested_items, is_code=True))


class PageBreak(TextParagraph): # TODO: make PB from automatic styles
    """!
    This is a kludge and has to be removed soon (after implementing the
    Styling Engine, but it is still unimplemented).
    """
    obj_type = PAGE_BREAK_TYPE
    nested_items = []

    def __init__(self, mmic):
        self.stylename = mmic.page_break_stylename

    def __str__(self):
        return ogl.mkemptytextp(self.stylename)


class Heading(OObject):
    """!
    Just a heading text paragraph.
    """
    obj_type = HEAD_TYPE

    def __init__(self, mmic, line):
        trig_pos = line.find(mmic.heading_trig)
        trig_end = mmic.heading_trig + SPACE
        trig_end_pos = line.find(trig_end, trig_pos) + len(mmic.heading_trig)
        trig_streak = line[trig_pos:trig_end_pos]
        self.lvl = get_heading_lvl(mmic, trig_streak)
        self.stylename = mmic.heading_stylename_template.format(lvl=self.lvl)
        super().__init__(mmic, line[trig_end_pos:])

    def __str__(self):
        return ogl.mktexth(self.lvl, self.stylename, super().__str__())


class Frame(OObject):
    """!
    Currently this is sort of a placeholder, which handles the outer frame
    of a Picture.
    """
    obj_type = FRAME_TYPE
    content = ""
    stylename = ""
    number = 0
    xsize = 0
    ysize = 0
    def __str__(self):
        return ogl.mkframe(self.content,
                            self.stylename,
                            self.mmic.frame_stylename,
                            self.number,
                            self.mmic.frame_anchor_type,
                            self.mmic.paragraph_width,
                            self.ysize)


class Picture(Frame): # TODO: improve picture handling
    """!
    A class that handles pictures. Currently the support is very basic.
    There is a lot of space for improvements.
    """
    obj_type = PIC_TYPE

    def __init__(self, mmic, line, number, md_source):
        self.mmic = mmic
        self.nested_items = list()
        self.separator = mmic.pic_prefix_separator
        self.stylename = mmic.pic_caption_stylename

        trig_b = line.find(mmic.pic_b_trig)
        trig_m = line.find(mmic.pic_m_trig, trig_b)
        trig_e = line.find(mmic.pic_e_trig, trig_m)

        self.path = PathHandler(line[trig_m + len(mmic.pic_m_trig):trig_e], md_source)
        self.name = self.path.path.name
        self.caption = prepare_text(mmic, line[trig_b + len(mmic.pic_b_trig):trig_m])
        if self.caption.isspace() or not self.caption:
            self.separator = ''
            self.caption = ''

        self.number = number
        self.mimetype = mts.guess_type(str(self.path))[0]

        with open(str(self.path), 'rb') as imgfile:
            if self.mmic.picture_name_is_hash:
                img = imgfile.read()
                name = hashlib.sha256(img).hexdigest() + self.path.path.suffix
            else:
                img = imgfile.read(32) # Just an arbitrary number (> 24)

        line_residue = line[trig_e + len(mmic.pic_e_trig):]
        if mmic.aspect_ratio_separator not in line_residue:
            # only PNG is supported when guessing image dimensions
            # others can become broken
            xpx = int.from_bytes(img[16:20], 'big')
            ypx = int.from_bytes(img[20:24], 'big')
            self.xsize, self.ysize = self.guess_dim(list((xpx,ypx)))
        else:
            aspect_ratio = line_residue.split(mmic.aspect_ratio_separator)
            self.xsize, self.ysize = self.guess_dim(aspect_ratio)

    def guess_dim(self, ar):
        """!
        Guess picture dimensions via aspect ratio ('ar' in funtion arguments).
        """
        xsize = 0
        ysize = 0
        ar[0] = int(ar[0])
        ar[1] = int(ar[1])
        #print (ar, ppcm)
        if len(ar) > 2:
            if ar[2] == cm_dim:
                xsize = ar[0]
                ysize = ar[1]
            elif ar[2] == px_dim:
                #print (ar, ppcm)
                xsize = ar[0] / self.mmic.ppcm
                ysize = ar[1] / self.mmic.ppcm
        else:
            xsize = self.mmic.paragraph_width * (ar[0]/(ar[0]+ar[1]))
            ysize = self.mmic.paragraph_width * (ar[1]/(ar[0]+ar[1]))
        return xsize, ysize

    def __str__(self):
        string = ""
        for i in self.nested_items:
            string += str(i)
        self.content = ogl.mkpicture(self.name,
                                self.mimetype,
                                self.caption,
                                self.mmic.pic_stylename,
                                self.number,
                                self.separator,
                                self.mmic.pic_caption_prefix,
                                self.mmic.pic_anchor_type,
                                self.xsize,
                                self.ysize)
        return super().__str__() + string

class ListItem(OObject): # just a wrapper for better understanding
    """!
    A wrapper class (just an OObject with different name).
    """
    obj_type = LIST_ITEM_TYPE

class List(OObject): # abstract class
    """!
    An abstract class which implements the most of methods which are used
    in inherited classes.
    """
    obj_type = LIST_TYPE
    trigger = ''
    lvl = 1
    stylename = ''

    def __init__(self, mmic, line='', lvl=1):
        log_fname(self)
        self.mmic = mmic
        self.nested_items = list()
        self.lvl = lvl
        line = rm_trig(line, self.trigger)
        self.nested_items.append(ListItem(self.mmic, TextParagraph(self.mmic, line)))
        log(self, f"Left the __init__ of {self.obj_type}")

    def is_item_of_this_list(self, line, type_of_list, lvl):
        """!
        Return True if type_of_list is equal to self.obj_type and
        lvl is equal to self.lvl.
        """
        log_fname(self)
        return type_of_list == self.obj_type and lvl == self.lvl

    def last_nested_obj(self):
        """!
        Return the last added item reference (pointer).
        """
        log_fname(self)
        return self.nested_items[-1].nested_items[-1]

    def get_last_nested_type(self): # get type of the last nested item in the list
        """!
        Get type of the last added item.
        """
        log_fname(self)
        if not self.nested_items:
            return ""
        return self.last_nested_obj().obj_type

    def get_last_nested_lvl(self): # get level
        log_fname(self)
        if not self.nested_items:
            return ""
        return self.last_nested_obj().lvl

    def add_new_nested_list(self, line, type_of_list, lvl):
        """!
        Add a freshly new list with a single item.
        """
        log_fname(self)
        log(self, type_of_list)
        if type_of_list == BUL_LIST_TYPE:
            self.nested_items.append(ListItem(self.mmic, ListBulleted(self.mmic, line, lvl)))
        elif type_of_list == NUM_LIST_TYPE:
            self.nested_items.append(ListItem(self.mmic, ListNumbered(self.mmic, line, lvl)))


    def add_item(self, line, type_of_list, lvl):
        """!
        Add item to the current list or delegate it to the nested object.
        """
        log_fname(self)
        if self.is_item_of_this_list(line, type_of_list, lvl):
#            log(self, f"Before removing trigger {self.trigger}:\n" + line)
            line = rm_trig(line, self.trigger)
#            log(self, f"After removing trigger {self.trigger}:\n" + line)
        last_type = self.get_last_nested_type()
        if lvl > self.lvl:
            if type_of_list == last_type or (self.get_last_nested_lvl() < lvl
                                        and last_type.startswith(LIST_TYPE)):
                self.last_nested_obj().add_item(line, type_of_list, lvl)
            else:
                self.add_new_nested_list(line, type_of_list, lvl)
        else:
            self.nested_items.append(ListItem(self.mmic, TextParagraph(self.mmic, line)))


    def append_paragraph(self, line):
        log_fname(self)
        if self.get_last_nested_type() == TEXT_TYPE:
            self.nested_items[-1].nested_items.append(TextParagraph(self.mmic, line))
        else:
            self.nested_items[-1].nested_items[-1].append_paragraph(line)

    def __str__(self):
        log_fname(self)
        return ogl.mkminlist(self.stylename, self.nested_items)

class ListBulleted(List):
    """!
    A class which handles bulleted lists.
    """
    obj_type = BUL_LIST_TYPE
    def __init__(self, mmic, line='', lvl=1):
        self.trigger = mmic.bul_list_trig
        self.stylename = mmic.bul_list_stylename
        super().__init__(mmic, line, lvl)


class ListNumbered(List):
    """!
    A class which handles numbered lists.
    """
    obj_type = NUM_LIST_TYPE
    def __init__(self, mmic, line='', lvl=1):
        self.trigger = mmic.num_list_trig
        self.stylename = mmic.num_list_stylename
        if line.find(self.trigger) > 0:
            line = line[line.find(self.trigger):]
        super().__init__(mmic, line, lvl)

    def add_item(self, line, type_of_list, lvl):
        if self.is_item_of_this_list(line, type_of_list, lvl):
            log(self, f"Before removing trigger in List N:\n{line}")
            line = line[line.find(self.trigger):]
            log(self, f"After removing trigger in List N:\n{line}")
        super().add_item(line, type_of_list, lvl)




# TableRow and Table classes
# Note: Originally these were from another project, so some extra
#       functionality can be an overkill for a table generator

class TableRow(OObject):
    """!
    Row class just for ease of use
    """
    obj_type = TROW_TYPE
    def __init__(self, mmic, columns):
        if type(columns[0]) is not TextParagraph:
            self.columns = [ TextParagraph(mmic, column, mmic.left_cell_text_style) for column in columns ]
        else:
            self.columns = columns[:]
        self.parent_table = ''
        self.mmic = mmic
        # ~ print ("MMIC:",mmic)
        self.nested_items = list()

    def set_parent_table(self, pt):
        self.parent_table = pt

    def get_column_widths(self, cw = [], precision = 4):
        cw = list()
        for i in self.columns:
            if type(i) is float:
                cw.append(len(f"{i:.{precision}}"))
            else:
                cw.append(len(str(i)))
        return cw

    def length (self):
        return len(self.columns)

    def __str__(self):
        return ogl.mktablerow(self)

    def __repr__(self):
        return f"{self.columns}"

    def copy(self):
        c = TableRow(self.mmic, self.columns[:])
        c.set_parent_table(self.parent_table)
        return c

    def __getitem__(self, number):
        return self.columns[number]


class Table(OObject):
    obj_type = TABLE_TYPE
    def __init__(self, mmic, parameters, precision=4):
        """
        Acceptable parameters values are:

        parameters = {
                                        "name"  :   "Some Table Name",
                                        "title" :   [...],
                                        "rows"  :   [[...],...]
                    }

        Or:

        parameters = {
                                        "name"  :   "Some Table Name",
                                        "title" :   TableRow(),
                                        "rows"  :   [TableRow(),...]
                     }

        Example:
        parameters = {
                                        "name"  :   "Table",
                                        "title" :   TableRow(),
                                        "rows"  :   [TableRow()]
                                    }
        """
        # ~ print(parameters)
        if type(parameters["title"]) is not TableRow:
            parameters["title"] = TableRow(mmic, parameters["title"])
        if type(parameters["rows"][0]) is not TableRow:
            for i, row in enumerate(parameters["rows"]):
                parameters["rows"][i] = TableRow(mmic, row)
        self.parameters = parameters.copy()

        self.parameters["title"] = self.parameters["title"].copy()
        for i, row in enumerate(self.parameters["rows"]):
            self.parameters["rows"][i] = row.copy()

        self.precision = precision
        if "name" in parameters:
            self.name = parameters["name"]
        else:
            self.name = "Table"
        if "rows" in parameters:
            self.rows = parameters["rows"][:]
        else:
            self.rows = list()
        if "title" in parameters:
            self.title = parameters["title"]
            self.title.set_parent_table(self)
        else:
            raise Exception("Any table must have a title row")
        # Check if rows have the same length and adjust title length if needed
        if self.rows:
            table_row_len = self.title.length()
            # ~ print(table_row_len)
            for i in self.rows:
                if i.length() != table_row_len:
                    # ~ print(i, i.length())
                    raise Exception("Length of all rows must be the same value")
                i.set_parent_table(self)
        self.column_widths = list()
        self.update_column_widths()
        # ~ print ("MMIC:",mmic)
        self.mmic = mmic
        self.nested_items = list()

    def get_row(self, keyindex, indexed_column = 0):
        if type(keyindex) is str:
            keyindex = self.get_column(indexed_column).index(keyindex)
        return self.rows[index]

    def get_column(self, keyindex):
        if type(keyindex) is str:
            keyindex = self.title.columns.index(keyindex)
        return [ row[keyindex] for row in self.rows ]

    def update_column_widths(self):
        max_cw = self.title.get_column_widths()
        #print(max_cw)
        for row in self.rows:
            for j, width in enumerate(row.get_column_widths(precision=self.precision)):
                if width > max_cw[j]:
                    max_cw[j] = width
        #print(max_cw)
        self.column_widths = max_cw[:]
                

    def __str__(self):
        return ogl.mktable(self)

    def copy(self):
        return Table(mmic, self.parameters)

    def __getitem__(self, key):
        if key == "title":
            return self.title
        return self.rows[key]
    
    def to_list(self, with_row_names = False):
        matrix = []
        for row in self.rows:
            if not with_row_names:
                matrix.append(row.columns[1:])
            else:
                matrix.append(row.columns)
            #print(matrix[-1])
        return matrix
        
    def get_matrix(self):
        return self.to_list(with_row_names=False)
        
    def append_row(self, row):
        if type(row) is not TableRow:
            row = TableRow(row)
        self.rows.append(row.copy)

