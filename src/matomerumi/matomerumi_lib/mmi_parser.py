# MMI v2.0
# Codename: Fir
# Copyright 2021 Fe-Ti
"""!
Parser module
"""

from .mmi_text_classes_and_functions import *


class MdParser:
    """!
    One class to parse them all.
    """

    def __init__(self, mmic, ifile, ifile_path, debug = True):
        self.mmic = mmic
        self.ifile = ifile
        self.ifile_path = ifile_path
        self.debug = debug
        self.chunks = list()         # document chunks
        self.embedded_pictures = list() # a list of pictures used in the document

        self.prev_type = ""

        self.bl_line_flag = True     # blank line flag

        # sort of a kludge
        self.skip_next_bl = False    # I like MD, but some stuff is annoying

        #pb_line_flag = False    # page break flag
        self.is_code_flag = False
        #is_ttpg_flag = False    # title page flag

        self.is_table_flag = False  # a flag for determining if we need
                                    # to accumulate lines as rows for parsing as
                                    # a simple table

        # a list for accumulating table rows
        self.table_row_accumulator = list()
        self.table_counter = 0

        self.style_flags = { # for generating automatic styles
            'has_bul_list'  : False,
            'has_num_list'  : False,
            'has_frame'     : False,
            'has_picture'   : False,
            'has_table'     : False,
        }

    def get_last_chunk_type(self):
        if not self.chunks: # if chunks is empty say nothing
            return ""
        return self.chunks[-1].obj_type # or get the previous obj. type

    def parse_file(self):
        for line in self.ifile:
            if line.startswith(self.mmic.switch_trig):
                self.set_chunk_flags(line)
            elif self.is_code_flag:
                self.chunks.append(CodeBlock(self.mmic, line))
            elif self.is_table_flag:
                if is_table(self.mmic, line):
                    self.table_row_accumulator.append(line)
                else:
                    table_params = self.parse_md_table()
                    self.chunks.append(TextParagraph(
                                                    self.mmic,
                                                    self.mmic.table_caption.format(num=self.table_counter),
                                                    self.mmic.table_caption_style
                                                    )
                                        )
                    self.chunks.append(Table(self.mmic, table_params))
                    self.chunks.append(TextParagraph(self.mmic))
                    self.is_table_flag = False
                    self.table_row_accumulator.clear()
                    self.parse_elements(line)   # it's possible that
                                                # the line contains something
            else:
                self.parse_elements(line)

    def set_chunk_flags(self, line): # TODO: make title page, raw xml and code blocks
        #line = rm_trig(line, mmic.switch_trig)
        self.is_code_flag = not self.is_code_flag


    def parse_elements(self, line):
        if line.isspace() or not line:
            if self.bl_line_flag and self.mmic.allow_empty_paragraphs:
                self.chunks.append(TextParagraph(self.mmic, line))
            if not self.skip_next_bl:
                self.bl_line_flag = True
            else:
                self.skip_next_bl = False
        elif is_table(self.mmic, line):
            self.style_flags['has_table'] = True
            self.is_table_flag = True
            self.table_row_accumulator.append(line)
        else:
            self.skip_next_bl = False
            log(self, f"Line is:\n{line}")
            line = line.expandtabs(tabsize=self.mmic.tab_size)
            if is_heading(self.mmic, line):
                self.chunks.append(Heading(self.mmic, line))
            elif is_page_break(self.mmic, line):
                self.chunks.append(PageBreak(self.mmic))
            elif is_picture(self.mmic, line):
                self.style_flags['has_frame'] = True
                self.style_flags['has_picture'] = True
                if self.mmic.pic_md_compat:
                    self.skip_next_bl = True # skip next blank line (MD compatibility issue)
                self.add_picture(line)
            elif is_bulleted_list_item(self.mmic, line):
                self.style_flags['has_bul_list'] = True
                self.add_list_item(line, BUL_LIST_TYPE)
            elif is_numbered_list_item(self.mmic, line):
                self.style_flags['has_num_list'] = True
                self.add_list_item(line, NUM_LIST_TYPE)
            else:
                self.append_text(line)
            self.bl_line_flag = False
           # self.pb_line_flag = False

    def add_picture(self, line):
        try:
            self.chunks.append(TextParagraph(self.mmic, [Picture(self.mmic, line, len(self.embedded_pictures) + 1, self.ifile_path)]))
            # note: embedded_pictures contains tuples of length == 2
            self.embedded_pictures.append((self.chunks[-1].nested_items[0].path,
                                            self.chunks[-1].nested_items[0].name))
        except FileNotFoundError:
            print(f"Can't open picture from '{line.strip()}'")

    def add_list_item(self, line, type_of_list):
        last_chunk_type = self.get_last_chunk_type()
        lvl = get_indent_lvl(self.mmic, line) + 1 # counting from 1
        log(self, f"List level is {lvl}")
        # if (line is the same list type
        # or we have a list embedded into another one)
        # then we add new list item
        # else we create new list
        if (last_chunk_type == type_of_list
                or (last_chunk_type.startswith(LIST_TYPE)
                        and lvl > self.chunks[-1].lvl)):
            self.chunks[-1].add_item(line, type_of_list, lvl)
        else:
            if type_of_list == BUL_LIST_TYPE:
                self.chunks.append(ListBulleted(self.mmic, line, lvl))
            elif type_of_list == NUM_LIST_TYPE:
                self.chunks.append(ListNumbered(self.mmic, line, lvl))

    def append_text(self, line):
        last_chunk_type = self.get_last_chunk_type()
        if last_chunk_type == TEXT_TYPE and not self.bl_line_flag:
            self.chunks[-1].append_line(line)
        elif last_chunk_type.startswith(LIST_TYPE) and (not self.bl_line_flag
                                                            or is_indented(self.mmic, line)):
            if self.bl_line_flag:
                self.chunks[-1].append_paragraph(line)
            else:
                self.chunks[-1].append_line(line)
        else:
            self.chunks.append(TextParagraph(self.mmic, line))

    def parse_md_table(self):
        tablerows = self.table_row_accumulator
        log(self, tablerows)
        title = tablerows[0].split(self.mmic.table_trig)
        while EMPTY_STR in title:
            title.remove(EMPTY_STR)
        # skipping 1 line
        rows = list()
        for row in tablerows[2:]:
            cols = row.split(self.mmic.table_trig)
            while EMPTY_STR in cols:
                cols.remove(EMPTY_STR)
            rows.append (cols)
            # ~ print(len(rows[-1]))
        self.table_counter += 1
        name = f"Table {self.table_counter}"
        params = {
                "name"  :   name,
                "title" :   title,
                "rows"  :   rows
                }
        # ~ print(params)
        return params

    def make_astyles(self):
        string = ''
        if self.style_flags['has_frame']:
            string += ast.mkframe_style(self.mmic.frame_stylename)
        if self.style_flags['has_picture']:
            string += ast.mkpic_style(self.mmic.pic_stylename)
        if self.style_flags['has_bul_list']:
            string += ast.mkbul_list_style(self.mmic.bul_list_stylename, self.mmic.list_props)
        if self.style_flags['has_num_list']:
            string += ast.mknum_list_style(self.mmic.num_list_stylename, self.mmic.list_props)
        if self.style_flags['has_table']:
            string += ast.mktable_styles(
                                        self.mmic.table_stylename,
                                        self.mmic.paragraph_width,
                                        self.mmic.left_cell_style,
                                        self.mmic.cell_style,
                                        self.mmic.right_cell_style,
                                        )
        return f'<office:automatic-styles>{string}</office:automatic-styles>'

    def compose_xml(self):
        #print(self.chunks)
        styles = self.make_astyles()
        xml_content = ''
        for i in self.chunks:
            xml_content += str(i)
        xml_content = ogl.mkdocbody(xml_content)
        return ogl.mkcontent_xml(styles, xml_content)

    # Debug printing
    def print_middle_node(self, node, prefix):
        for i, elem in enumerate(node.nested_items):
            if i < (len(node.nested_items) - 1):
                print(prefix + "┣", elem.__repr__())
                self.print_middle_node(elem, prefix + "┃ ")
            else:
                print(prefix + "┗", elem.__repr__())
                self.print_last_node(elem, prefix + "  ")
    def print_last_node(self, node, prefix):
        for i, elem in enumerate(node.nested_items):
            if i < (len(node.nested_items) - 1):
                print(prefix + "┣", elem.__repr__())
                self.print_middle_node(elem, prefix + "┃ ")
            else:
                print(prefix + "┗", elem.__repr__())
                self.print_last_node(elem, prefix + "  ")
    def print_tree(self):
        print("DOCUMENT ROOT")
        for i, elem in enumerate(self.chunks):
            if i < (len(self.chunks) - 1):
                print("┣", elem.__repr__())
                self.print_middle_node(elem, "┃ ")
            else:
                print("┗", elem.__repr__())
                self.print_last_node(elem, "  ")
