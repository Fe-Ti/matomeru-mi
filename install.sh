#!/bin/bash
DEFAULT_ARG_1="/usr/lib/matomeru-mi"
DEFAULT_ARG_2="/usr/bin/matomeru-mi"
if [ ! $1 ]
then
        echo "Enter path where matomeru_lib will be installed: [${DEFAULT_ARG_1}]"
        read PATH_TO_MMI_MAIN
        if [ ! $PATH_TO_MMI_MAIN ]
        then
            PATH_TO_MMI_MAIN=${DEFAULT_ARG_1}
        fi
else
        PATH_TO_MMI_MAIN=$1
fi

if [ ! $2 ]
then
        echo "Enter path where script for running MMI will be installed: [${DEFAULT_ARG_2}]"
        read MMI_SHELL_RUNNER
        if [ ! $MMI_SHELL_RUNNER ]
        then
            MMI_SHELL_RUNNER=${DEFAULT_ARG_2}
        fi
else
        MMI_SHELL_RUNNER=$2
fi


if type python3
then
    echo "Using python3 as Python 3 interpreter"
    PYINT=$(type -pfa python3)
else
    if type python
    then
        echo "Using python as Python 3 interpreter"
        PYINT=$(type -pfa python)
    else
        echo "Error! Can't guess what is alias for Python 3 interpreter"
        exit
    fi
fi

cp -R --verbose src/matomerumi ${PATH_TO_MMI_MAIN}

echo "#!/bin/sh
${PYINT} ${PATH_TO_MMI_MAIN}/__main__.py \"\$@\"" > $MMI_SHELL_RUNNER
# next is just for compatibility with pip packaging
sed -i s/\.matomerumi_lib/matomerumi_lib/ ${PATH_TO_MMI_MAIN}/__main__.py
chmod +x $MMI_SHELL_RUNNER

echo "#!/bin/sh
rm -rvf ${PATH_TO_MMI_MAIN}
rm -vf ${MMI_SHELL_RUNNER}
" > uninstall.sh
chmod +x uninstall.sh
echo "For uninstalling run uninstall.sh or commands below
rm -rvf ${PATH_TO_MMI_MAIN}
rm -vf ${MMI_SHELL_RUNNER}
"

