# Syntax
The syntax can vary depending on a config file. By default everything is set as follows.
```
Simple text
paragraph

Another
paragraph
```
Headings use # as triggers by default.
```
# Level 1 heading
## Level 2 heading
########## And now level 10
```
The list are like they are in Markdown (currently only one style at a time is supported).
```
- The first bulleted list
    1. Embedded numbered list
- The same bulleted
1. Another numbered list
2. The same numbered list
```
The page break is using this style of trigger:
```
----
- - - -
------this is page break too
----- I think it's not a bug but a feature
```

Pictures are handled via this:
```
![name](path)horizontal:vertical
![name](path)horizontal:vertical:aspect_ratio_units
```
Where horizontal:vertical means aspect ratio like 1:2 and units can be nothing, cm or px.
Also PNGs can be handled automatically:
```
![png name](path to png)
```
Also it is possible to make links to pictures like so:
```
%%%picture_number
```

Here are some examples of picture handling (when using MMI).

 - PNG, auto (picture %%%1)

![Demo PNG(License CC BY-NC-SA)](demo-images/logo_1.png)

 - JPG, a/r == 4:1 (picture %%%2)

![Demo JPG(License CC BY-NC-SA)](demo-images/logo_1.jpg)4:1

 - SVG, a/r == 1:1 (picture %%%3)

![Dome SVG(License CC BY-NC-SA)](demo-images/logo_1.svg)1:1


